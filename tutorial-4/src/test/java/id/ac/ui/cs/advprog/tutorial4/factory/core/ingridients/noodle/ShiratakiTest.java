package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShiratakiTest {
    private Class<?> shiratakiClass;
    private Shirataki shirataki;

    @BeforeEach
    public void setUp() throws Exception {
        shiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki");
        shirataki = new Shirataki();
    }

    @Test
    public void testShiratakiHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = shiratakiClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testShiratakiMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Snevnezha Shirataki Noodles...",  shirataki.getDescription());
    }
}
