package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceImplTest {

    @InjectMocks
    OrderServiceImpl orderService = new OrderServiceImpl();

    @Test
    public void testOrderServiceOrderDrinkCorrectly() {
        orderService.orderADrink("A drink");
        assertEquals("A drink", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderServiceOrderFoodCorrectly() {
        orderService.orderAFood("A food");
        assertEquals("A food", orderService.getFood().getFood());
    }
}
