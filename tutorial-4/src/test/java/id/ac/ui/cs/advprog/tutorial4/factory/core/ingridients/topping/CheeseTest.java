package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CheeseTest {
    private Class<?> cheeseClass;
    private Cheese cheese;

    @BeforeEach
    public void setUp() throws Exception {
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese");
        cheese = new Cheese();
    }

    @Test
    public void testCheeseHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = cheeseClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testCheeseMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Shredded Cheese Topping...",  cheese.getDescription());
    }
}
