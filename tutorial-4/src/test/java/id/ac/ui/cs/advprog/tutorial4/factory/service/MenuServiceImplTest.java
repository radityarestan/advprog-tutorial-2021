package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceImplTest {
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setUp() throws Exception {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceImplAddingMenuCorrectly() throws Exception {
        List<Menu> menus = menuService.getMenus();
        assertEquals(4, menus.size());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba",
                menus.get(0).getClass().getTypeName());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen",
                menus.get(1).getClass().getTypeName());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon",
                menus.get(2).getClass().getTypeName());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki",
                menus.get(3).getClass().getTypeName());
    }
}
