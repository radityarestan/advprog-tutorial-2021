package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SaltyTest {
    private Class<?> saltyClass;
    private Salty salty;

    @BeforeEach
    public void setUp() throws Exception {
        saltyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
        salty = new Salty();
    }

    @Test
    public void testSaltyHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = saltyClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testSaltyMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding a pinch of salt...", salty.getDescription());
    }
}
