package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BoiledEggTest {
    private Class<?> boiledEggClass;
    private BoiledEgg boiledEgg;

    @BeforeEach
    public void setUp() throws Exception {
        boiledEggClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
        boiledEgg = new BoiledEgg();
    }

    @Test
    public void testBoiledEggHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = boiledEggClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testBoiledEggMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Guahuan Boiled Egg Topping",  boiledEgg.getDescription());
    }
}
