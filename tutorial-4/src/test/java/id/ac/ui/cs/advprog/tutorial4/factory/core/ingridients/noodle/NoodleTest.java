package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NoodleTest {
    private Class<?> noodleClass;

    @BeforeEach
    public void setup() throws Exception {
        noodleClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle");
    }

    @Test
    public void testNoodleIsAPublicInterface() {
        int classModifier = noodleClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }

    @Test
    public void testNoodleHasGetDescriptionMethod() throws Exception {
        Method buildFlavor = noodleClass.getDeclaredMethod("getDescription");
        int methodModifiers = buildFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, buildFlavor.getParameterCount());
    }
}
