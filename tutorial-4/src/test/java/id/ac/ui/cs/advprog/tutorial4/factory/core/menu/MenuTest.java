package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class MenuTest {
    private Class<?> menuClass;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsPublicAndAbstractClass() throws Exception {
        int methodModifier = menuClass.getModifiers();
        assertTrue(Modifier.isAbstract(methodModifier));
        assertTrue(Modifier.isPublic(methodModifier));
    }

}
