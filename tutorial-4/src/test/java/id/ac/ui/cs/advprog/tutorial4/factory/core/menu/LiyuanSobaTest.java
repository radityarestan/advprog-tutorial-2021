package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaTest {
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menu = new LiyuanSoba("Dummy Liyuan Soba");
    }

    @Test
    public void testLiyuanSobaNameOutput() throws Exception {
        assertEquals("Dummy Liyuan Soba", menu.getName());
    }

    @Test
    public void testLiyuanSobaNoodleOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba",
                menu.getNoodle().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaMeatOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef",
                menu.getMeat().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaToppingOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom",
                menu.getTopping().getClass().getTypeName());
    }

    @Test
    public void testLiyuanSobaFlavorOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet",
                menu.getFlavor().getClass().getTypeName());
    }
}
