package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChickenTest {
    private Class<?> chickenClass;
    private Chicken chicken;

    @BeforeEach
    public void setUp() throws Exception {
        chickenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
        chicken = new Chicken();
    }

    @Test
    public void testChickenHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = chickenClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testChickenMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Wintervale Chicken Meat...",  chicken.getDescription());
    }
}
