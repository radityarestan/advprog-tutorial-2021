package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FishTest {
    private Class<?> fishClass;
    private Fish fish;

    @BeforeEach
    public void setUp() throws Exception {
        fishClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish");
        fish = new Fish();
    }

    @Test
    public void testFishHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = fishClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testFishMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Zhangyun Salmon Fish Meat...",  fish.getDescription());
    }
}
