package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @BeforeEach
    public void setUp() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService");
    }

    @Test
    public void testMenuServiceIsAPublicInterface() {
        int classModifier = menuServiceClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getMenus.getParameterCount());
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", String.class, String.class);
        int methodModifiers = createMenu.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(2, createMenu.getParameterCount());
    }
}
