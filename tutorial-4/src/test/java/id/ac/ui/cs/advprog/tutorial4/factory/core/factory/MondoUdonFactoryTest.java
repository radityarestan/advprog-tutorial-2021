package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;
    private MondoUdonFactory mondoUdonFactory;

    @BeforeEach
    public void setUp() throws Exception{
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryIsPublicClass() {
        assertTrue(Modifier.isPublic(mondoUdonFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideBuildFlavorMethod() throws Exception {
        Method buildFlavor = mondoUdonFactoryClass.getDeclaredMethod("buildFlavor");

        assertTrue(Modifier.isPublic(buildFlavor.getModifiers()));
        assertEquals(0, buildFlavor.getParameterCount());
    }

    @Test
    public void testMondoUdonFactoryOverrideBuildMeatMethod() throws Exception {
        Method buildMeat = mondoUdonFactoryClass.getDeclaredMethod("buildMeat");

        assertTrue(Modifier.isPublic(buildMeat.getModifiers()));
        assertEquals(0, buildMeat.getParameterCount());
    }

    @Test
    public void testMondoUdonFactoryOverrideBuildNoodleMethod() throws Exception {
        Method buildNoodle = mondoUdonFactoryClass.getDeclaredMethod("buildNoodle");

        assertTrue(Modifier.isPublic(buildNoodle.getModifiers()));
        assertEquals(0, buildNoodle.getParameterCount());
    }

    @Test
    public void testMondoUdonFactoryOverrideBuildToppingMethod() throws Exception {
        Method buildTopping = mondoUdonFactoryClass.getDeclaredMethod("buildTopping");

        assertTrue(Modifier.isPublic(buildTopping.getModifiers()));
        assertEquals(0, buildTopping.getParameterCount());
    }

    @Test
    public void testMondoUdonFactoryBuildFlavorMethodMustBeReturnSalty() throws Exception {
        assertEquals("Adding a pinch of salt...", mondoUdonFactory.buildFlavor().getDescription());
    }

    @Test
    public void testMondoUdonFactoryBuildMeatMethodMustBeReturnChicken() throws Exception {
        assertEquals("Adding Wintervale Chicken Meat...", mondoUdonFactory.buildMeat().getDescription());
    }

    @Test
    public void testMondoUdonFactoryBuildNoodleMethodMustBeReturnUdon() throws Exception {
        assertEquals("Adding Mondo Udon Noodles...", mondoUdonFactory.buildNoodle().getDescription());
    }

    @Test
    public void testMondoUdonFactoryBuildToppingMethodMustBeReturnCheese() throws Exception {
        assertEquals("Adding Shredded Cheese Topping...", mondoUdonFactory.buildTopping().getDescription());
    }
}
