package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PorkTest {
    private Class<?> porkClass;
    private Pork pork;

    @BeforeEach
    public void setUp() throws Exception {
        porkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
        pork = new Pork();
    }

    @Test
    public void testPorkHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = porkClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testPorkMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Tian Xu Pork Meat...",  pork.getDescription());
    }
}
