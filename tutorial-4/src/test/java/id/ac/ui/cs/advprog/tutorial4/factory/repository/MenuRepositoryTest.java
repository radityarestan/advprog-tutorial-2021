package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() throws Exception {
        menuRepository = new MenuRepository();
    }

    @Test
    public void testMenuRepositoryListIsGrowingUp() throws Exception {
        Menu menu1 = new LiyuanSoba("Dummy Liyuan Soba");
        menuRepository.add(menu1);
        assertEquals(1, menuRepository.getMenus().size());

        Menu menu2 = new MondoUdon("Dummy Mondo Udon");
        menuRepository.add(menu2);
        assertEquals(2, menuRepository.getMenus().size());
    }
}
