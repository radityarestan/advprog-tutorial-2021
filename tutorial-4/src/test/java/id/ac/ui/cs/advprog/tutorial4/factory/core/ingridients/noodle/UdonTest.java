package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UdonTest {
    private Class<?> udonClass;
    private Udon udon;

    @BeforeEach
    public void setUp() throws Exception {
        udonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
        udon = new Udon();
    }

    @Test
    public void testUdonHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = udonClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testUdonMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Mondo Udon Noodles...",  udon.getDescription());
    }
}
