package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiClass;
    private SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeEach
    public void setUp() throws Exception{
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory");
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsPublicClass() {
        assertTrue(Modifier.isPublic(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideBuildFlavorMethod() throws Exception {
        Method buildFlavor = snevnezhaShiratakiClass.getDeclaredMethod("buildFlavor");

        assertTrue(Modifier.isPublic(buildFlavor.getModifiers()));
        assertEquals(0, buildFlavor.getParameterCount());
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideBuildMeatMethod() throws Exception {
        Method buildMeat = snevnezhaShiratakiClass.getDeclaredMethod("buildMeat");

        assertTrue(Modifier.isPublic(buildMeat.getModifiers()));
        assertEquals(0, buildMeat.getParameterCount());
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideBuildNoodleMethod() throws Exception {
        Method buildNoodle = snevnezhaShiratakiClass.getDeclaredMethod("buildNoodle");

        assertTrue(Modifier.isPublic(buildNoodle.getModifiers()));
        assertEquals(0, buildNoodle.getParameterCount());
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideBuildToppingMethod() throws Exception {
        Method buildTopping = snevnezhaShiratakiClass.getDeclaredMethod("buildTopping");

        assertTrue(Modifier.isPublic(buildTopping.getModifiers()));
        assertEquals(0, buildTopping.getParameterCount());
    }

    @Test
    public void testSnevnezhaShiratakiFactoryBuildFlavorMethodMustBeReturnUmami() throws Exception {
        assertEquals("Adding WanPlus Specialty MSG flavoring...", snevnezhaShiratakiFactory.buildFlavor().getDescription());
    }

    @Test
    public void testSnevnezhaShiratakiFactoryBuildMeatMethodMustBeReturnFish() throws Exception {
        assertEquals("Adding Zhangyun Salmon Fish Meat...", snevnezhaShiratakiFactory.buildMeat().getDescription());
    }

    @Test
    public void testSnevnezhaShiratakiFactoryBuildNoodleMethodMustBeReturnShirataki() throws Exception {
        assertEquals("Adding Snevnezha Shirataki Noodles...", snevnezhaShiratakiFactory.buildNoodle().getDescription());
    }

    @Test
    public void testSnevnezhaShiratakiFactoryBuildToppingMethodMustBeReturnFlower() throws Exception {
        assertEquals("Adding Xinqin Flower Topping...", snevnezhaShiratakiFactory.buildTopping().getDescription());
    }
}
