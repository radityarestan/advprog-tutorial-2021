package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BeefTest {
    private Class<?> beefClass;
    private Beef beef;

    @BeforeEach
    public void setUp() throws Exception {
        beefClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef");
        beef = new Beef();
    }

    @Test
    public void testBeefHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = beefClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testBeefMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Maro Beef Meat...", beef.getDescription());
    }
}
