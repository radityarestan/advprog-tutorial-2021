package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenFactoryTest {
    private Class<?> inuzumaRamenFactoryClass;
    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    public void setUp() throws Exception{
        inuzumaRamenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory");
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    public void testInuzumaRamenFactoryIsPublicClass() {
        assertTrue(Modifier.isPublic(inuzumaRamenFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideBuildFlavorMethod() throws Exception {
        Method buildFlavor = inuzumaRamenFactoryClass.getDeclaredMethod("buildFlavor");

        assertTrue(Modifier.isPublic(buildFlavor.getModifiers()));
        assertEquals(0, buildFlavor.getParameterCount());
    }

    @Test
    public void testInuzumaRamenFactoryOverrideBuildMeatMethod() throws Exception {
        Method buildMeat = inuzumaRamenFactoryClass.getDeclaredMethod("buildMeat");

        assertTrue(Modifier.isPublic(buildMeat.getModifiers()));
        assertEquals(0, buildMeat.getParameterCount());
    }

    @Test
    public void testInuzumaRamenFactoryOverrideBuildNoodleMethod() throws Exception {
        Method buildNoodle = inuzumaRamenFactoryClass.getDeclaredMethod("buildNoodle");

        assertTrue(Modifier.isPublic(buildNoodle.getModifiers()));
        assertEquals(0, buildNoodle.getParameterCount());
    }

    @Test
    public void testInuzumaRamenFactoryOverrideBuildToppingMethod() throws Exception {
        Method buildTopping = inuzumaRamenFactoryClass.getDeclaredMethod("buildTopping");

        assertTrue(Modifier.isPublic(buildTopping.getModifiers()));
        assertEquals(0, buildTopping.getParameterCount());
    }

    @Test
    public void testInuzumaRamenFactoryBuildFlavorMethodMustBeReturnSpicy() throws Exception {
        assertEquals("Adding Liyuan Chili Powder...", inuzumaRamenFactory.buildFlavor().getDescription());
    }

    @Test
    public void testInuzumaRamenFactoryBuildMeatMethodMustBeReturnPork() throws Exception {
        assertEquals("Adding Tian Xu Pork Meat...", inuzumaRamenFactory.buildMeat().getDescription());
    }

    @Test
    public void testInuzumaRamenFactoryBuildNoodleMethodMustBeReturnSpicy() throws Exception {
        assertEquals("Adding Inuzuma Ramen Noodles...", inuzumaRamenFactory.buildNoodle().getDescription());
    }

    @Test
    public void testInuzumaRamenFactoryBuildToppingMethodMustBeReturnSpicy() throws Exception {
        assertEquals("Adding Guahuan Boiled Egg Topping", inuzumaRamenFactory.buildTopping().getDescription());
    }
}
