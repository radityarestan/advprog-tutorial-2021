package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonTest {
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menu = new MondoUdon("Dummy Mondo Udon");
    }

    @Test
    public void testMondoUdonNameOutput() throws Exception {
        assertEquals("Dummy Mondo Udon", menu.getName());
    }

    @Test
    public void testMondoUdonNoodleOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon",
                menu.getNoodle().getClass().getTypeName());
    }

    @Test
    public void testMondoUdonMeatOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken",
                menu.getMeat().getClass().getTypeName());
    }

    @Test
    public void testMondoUdonToppingOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese",
                menu.getTopping().getClass().getTypeName());
    }

    @Test
    public void testMondoUdonFlavorOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty",
                menu.getFlavor().getClass().getTypeName());
    }
}
