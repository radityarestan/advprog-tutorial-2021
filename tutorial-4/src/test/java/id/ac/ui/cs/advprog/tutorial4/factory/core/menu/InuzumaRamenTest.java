package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InuzumaRamenTest {
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menu = new InuzumaRamen("Dummy Inuzuma Ramen");
    }

    @Test
    public void testInuzumaRamenNameOutput() throws Exception {
        assertEquals("Dummy Inuzuma Ramen", menu.getName());
    }

    @Test
    public void testInuzumaRamenNoodleOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen",
                menu.getNoodle().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenMeatOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork",
                menu.getMeat().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenToppingOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg",
                menu.getTopping().getClass().getTypeName());
    }

    @Test
    public void testInuzumaRamenFlavorOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy",
                menu.getFlavor().getClass().getTypeName());
    }
}
