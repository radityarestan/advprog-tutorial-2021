package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaFactoryTest {
    private Class<?> liyuanSobaFactoryClass;
    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    public void setUp() throws Exception{
        liyuanSobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory");
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryIsPublicClass() {
        assertTrue(Modifier.isPublic(liyuanSobaFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideBuildFlavorMethod() throws Exception {
        Method buildFlavor = liyuanSobaFactoryClass.getDeclaredMethod("buildFlavor");

        assertTrue(Modifier.isPublic(buildFlavor.getModifiers()));
        assertEquals(0, buildFlavor.getParameterCount());
    }

    @Test
    public void testLiyuanSobaFactoryOverrideBuildMeatMethod() throws Exception {
        Method buildMeat = liyuanSobaFactoryClass.getDeclaredMethod("buildMeat");

        assertTrue(Modifier.isPublic(buildMeat.getModifiers()));
        assertEquals(0, buildMeat.getParameterCount());
    }

    @Test
    public void testLiyuanSobaFactoryOverrideBuildNoodleMethod() throws Exception {
        Method buildNoodle = liyuanSobaFactoryClass.getDeclaredMethod("buildNoodle");

        assertTrue(Modifier.isPublic(buildNoodle.getModifiers()));
        assertEquals(0, buildNoodle.getParameterCount());
    }

    @Test
    public void testLiyuanSobaFactoryOverrideBuildToppingMethod() throws Exception {
        Method buildTopping = liyuanSobaFactoryClass.getDeclaredMethod("buildTopping");

        assertTrue(Modifier.isPublic(buildTopping.getModifiers()));
        assertEquals(0, buildTopping.getParameterCount());
    }

    @Test
    public void testLiyuanSobaFactoryBuildFlavorMethodMustBeReturnSweet() throws Exception {
        assertEquals("Adding a dash of Sweet Soy Sauce...", liyuanSobaFactory.buildFlavor().getDescription());
    }

    @Test
    public void testLiyuanSobaFactoryBuildMeatMethodMustBeReturnBeef() throws Exception {
        assertEquals("Adding Maro Beef Meat...", liyuanSobaFactory.buildMeat().getDescription());
    }

    @Test
    public void testLiyuanSobaFactoryBuildNoodleMethodMustBeReturnSoba() throws Exception {
        assertEquals("Adding Liyuan Soba Noodles...", liyuanSobaFactory.buildNoodle().getDescription());
    }

    @Test
    public void testLiyuanSobaFactoryBuildToppingMethodMustBeReturnMushroom() throws Exception {
        assertEquals("Adding Shiitake Mushroom Topping...", liyuanSobaFactory.buildTopping().getDescription());
    }
}
