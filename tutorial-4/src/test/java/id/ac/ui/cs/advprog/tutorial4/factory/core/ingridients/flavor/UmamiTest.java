package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UmamiTest {
    private Class<?> umamiClass;
    private Umami umami;

    @BeforeEach
    public void setUp() throws Exception {
        umamiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
        umami = new Umami();
    }

    @Test
    public void testUmamiHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = umamiClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testUmamiMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }
}
