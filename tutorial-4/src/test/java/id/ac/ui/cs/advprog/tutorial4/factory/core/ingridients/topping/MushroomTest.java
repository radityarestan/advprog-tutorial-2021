package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MushroomTest {
    private Class<?> mushroomClass;
    private Mushroom mushroom;

    @BeforeEach
    public void setUp() throws Exception {
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
        mushroom = new Mushroom();
    }

    @Test
    public void testMushroomHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = mushroomClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testMushroomMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Shiitake Mushroom Topping...",  mushroom.getDescription());
    }
}
