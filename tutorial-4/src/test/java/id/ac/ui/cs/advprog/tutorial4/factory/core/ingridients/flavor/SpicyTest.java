package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SpicyTest {
    private Class<?> spicyClass;
    private Spicy spicy;

    @BeforeEach
    public void setUp() throws Exception {
        spicyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy");
        spicy = new Spicy();
    }

    @Test
    public void testSpicyHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = spicyClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testSpicyMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Liyuan Chili Powder...", spicy.getDescription());
    }
}
