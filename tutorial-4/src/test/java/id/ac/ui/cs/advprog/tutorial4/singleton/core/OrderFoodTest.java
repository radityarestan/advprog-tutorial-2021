package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderFoodTest {
    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testOrderFoodCreateObjectShouldBeIdentical() {
        OrderFood orderFood2 = OrderFood.getInstance();
        assertEquals(orderFood, orderFood2);
    }

    @Test
    public void testOrderFoodOrderedAFoodCorrectly() {
        orderFood.setFood("Dummy Food");
        assertEquals("Dummy Food", orderFood.getFood());
        assertEquals("Dummy Food", orderFood.toString());
    }


}
