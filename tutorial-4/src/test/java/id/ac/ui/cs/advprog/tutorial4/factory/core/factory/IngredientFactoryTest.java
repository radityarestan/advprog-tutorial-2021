package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IngredientFactoryTest {
    private Class<?> ingredientFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        ingredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory");
    }

    @Test
    public void testSpellIsAPublicInterface() {
        int classModifier = ingredientFactoryClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }

    @Test
    public void testIngredientFactoryHasBuildFlavorMethod() throws Exception {
        Method buildFlavor = ingredientFactoryClass.getDeclaredMethod("buildFlavor");
        int methodModifiers = buildFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, buildFlavor.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasBuildMeatMethod() throws Exception {
        Method buildMeat = ingredientFactoryClass.getDeclaredMethod("buildMeat");
        int methodModifier = buildMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifier));
        assertTrue(Modifier.isAbstract(methodModifier));
        assertEquals(0, buildMeat.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasBuildNoodleMethod() throws Exception {
        Method buildNoodle = ingredientFactoryClass.getDeclaredMethod("buildNoodle");
        int methodModifier = buildNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifier));
        assertTrue(Modifier.isAbstract(methodModifier));
        assertEquals(0, buildNoodle.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasBuildToppingMethod() throws Exception {
        Method buildTopping = ingredientFactoryClass.getDeclaredMethod("buildTopping");
        int methodModifier = buildTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifier));
        assertTrue(Modifier.isAbstract(methodModifier));
        assertEquals(0, buildTopping.getParameterCount());
    }
}
