package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SobaTest {
    private Class<?> sobaClass;
    private Soba soba;

    @BeforeEach
    public void setUp() throws Exception {
        sobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba");
        soba = new Soba();
    }

    @Test
    public void testSobaHasOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = sobaClass.getDeclaredMethod("getDescription");
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
        assertEquals("java.lang.String", getDescription.getReturnType().getTypeName());
    }

    @Test
    public void testSobaMustBeReturnTheRightString() throws Exception {
        assertEquals("Adding Liyuan Soba Noodles...",  soba.getDescription());
    }
}
