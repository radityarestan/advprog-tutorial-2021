package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiTest {
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menu = new SnevnezhaShirataki("Dummy Snevnezha Shirataki");
    }

    @Test
    public void testSnevnezhaShiratakiNameOutput() throws Exception {
        assertEquals("Dummy Snevnezha Shirataki", menu.getName());
    }

    @Test
    public void testSnevnezhaShiratakiNoodleOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki",
                menu.getNoodle().getClass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiMeatOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish",
                menu.getMeat().getClass().getTypeName());
    }

    @Test
    public void testMSnevnezhaShiratakiToppingOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower",
                menu.getTopping().getClass().getTypeName());
    }

    @Test
    public void testSnevnezhaShiratakiFlavorOutput() throws Exception {
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami",
                menu.getFlavor().getClass().getTypeName());
    }
}
