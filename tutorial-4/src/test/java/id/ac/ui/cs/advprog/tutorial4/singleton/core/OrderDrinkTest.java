package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.ExceptionUtils;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.*;

public class OrderDrinkTest {

    @InjectMocks
    private OrderDrink orderDrink = OrderDrink.getInstance();

    @Test
    public void testOrderDrinkCreateObjectShouldBeIdentical() {
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertEquals(orderDrink, orderDrink2);
    }

    @Test
    public void testOrderDrinkOrderedADrinkCorrectly() {
        orderDrink.setDrink("Dummy Drink");
        assertEquals("Dummy Drink", orderDrink.getDrink());
        assertEquals("Dummy Drink", orderDrink.toString());
    }
}
