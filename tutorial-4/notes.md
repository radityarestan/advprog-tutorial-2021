Lazy vs Eager Instantiation

Perbedaan mendasar keduanya adalah momen pemanggillannya. Lazy Instantation
dipanggil ketika **dibutuhkan**, sedangkan Eager Instantiation **dibuat sesegara mungkin (diawal)**.

Lazy Instantiation   
Keuntungan
- Menghemat memori karena hanya dibuat ketika benar-benar dibutuhkan  

Kerugian
- Ketika ada kasus yang membuat objek dipanggil bersamaan dalam suatu waktu akan terjadi workload yang besar,
 apalagi ia perlu melakukan pengecekan apa objek sudah dibuat atau belum.

Eager Instantiation  
Keuntungan
- Ketika membutuhkan objek berkali-kali, tidak perlu membuatnya berkali-kali juga.
Karena sudah dinstantiasi di awal  
  
Kerugian
- Tentu Penggunaan memori di awal akan sangat besar karena sudah dibuat dulu (belum tentu dipakai).