package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private Log log;
    private Log anotherLog;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("2106292982", "Bambang", "Bambang@gmail.com", "3.0", "081234567890");
        log = new Log("2021/04/04 20:00", "2021/04/04 21:00", "Tutorial 1");
        anotherLog = new Log("2021/04/05 20:00", "2021/04/05 21:00", "Tutorial 2");
    }

    @Test
    public void testLogServiceCreateLog() throws Exception {
        mahasiswa.setLogList(new ArrayList<>());
        when(mahasiswaRepository.findByNpm(anyString())).thenReturn(mahasiswa);
        Log logResult = logService.createLog(mahasiswa.getNpm(), log);
        assertEquals(1, mahasiswa.getLogList().size());
        assertEquals("Tutorial 1", logResult.getDescription());
    }

    @Test
    public void testLogServiceGetLogOfMahasiswa() throws Exception {
        List<Log> logList = new ArrayList<>();

        logList.add(log);
        logList.add(anotherLog);

        mahasiswa.setLogList(logList);

        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        List<Log> logsResult = (List) logService.getLogOfMahasiswa(mahasiswa.getNpm());

        assertEquals(2, logsResult.size());
    }

    @Test
    public void testLogServiceGetMonthlyLog() throws Exception {
        List<Log> logList = new ArrayList<>();

        logList.add(log);
        logList.add(anotherLog);

        mahasiswa.setLogList(logList);
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        LogSummary logSummary = logService.getMonthlyLog(mahasiswa.getNpm(), "April");
        assertEquals(2.0, logSummary.getWorkingHours());
    }

    @Test
    public void testLogServiceGetAllLogSummary() throws Exception {
        List<Log> logList = new ArrayList<>();
        Log anotherLogAgain = new Log("2021/05/05 20:00", "2021/05/05 21:00", "Tutorial 3");
        logList.add(log);
        logList.add(anotherLog);
        logList.add(anotherLogAgain);

        mahasiswa.setLogList(logList);
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        List<LogSummary> logSummaryResult = (List) logService.getAllLogSummaryByMahasiswa(mahasiswa.getNpm());

        assertEquals(2, logSummaryResult.size());
    }

    @Test
    public void testLogServiceGetLogById() throws Exception {
        when(logRepository.findById(log.getIdLog())).thenReturn(log);

        Log logResult = logService.getLogByID(log.getIdLog());

        assertEquals("Tutorial 1", logResult.getDescription());
    }

    @Test
    public void testLogServiceUpdateLog(){
        Log updatedLog = new Log("2021/04/04 20:00", "2021/04/04 21:00", "Tutorial 1 Lab");
        when(logRepository.findById(log.getIdLog())).thenReturn(log);
        Log result = logService.updateLog(log.getIdLog(), updatedLog);
        assertEquals("Tutorial 1 Lab", result.getDescription());
    }

    @Test
    public void testLogServiceDeleteLog(){
        mahasiswa.setLogList(new ArrayList<>());

        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        logService.createLog(mahasiswa.getNpm(), log);

        logService.deleteLogByID(log.getIdLog());
        Log logResult = logService.getLogByID(log.getIdLog());
        assertEquals(null, logResult);
    }

}
