package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class RegisterAsdosServiceImplTest {

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @InjectMocks
    private RegisterAsdosServiceImpl registerAsdosService;

    private Mahasiswa mahasiswa;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("2106292982", "Bambang", "Bambang@gmail.com", "3.0", "081234567890");
        mataKuliah = new MataKuliah("CSUI0000", "POK", "Ilmu Komputer");
    }

    @Test
    public void testRegisterAsdosServiceToRegisterAnAssistant() throws Exception {
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahRepository.findByKodeMatkul(mataKuliah.getKodeMatkul())).thenReturn(mataKuliah);

        mataKuliah.setMahasiswaList(new ArrayList<>());
        registerAsdosService.registerToBecomeAsdos(mahasiswa.getNpm(), mataKuliah.getKodeMatkul());

        assertEquals(mataKuliah.getKodeMatkul(), mahasiswa.getMataKuliah().getKodeMatkul());
    }
}
