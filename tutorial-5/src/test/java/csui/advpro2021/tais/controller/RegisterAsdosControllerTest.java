package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.RegisterAsdosServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = RegisterAsdosController.class)
public class RegisterAsdosControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private RegisterAsdosServiceImpl registerAsdosService;

    private Mahasiswa mahasiswa;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("2106292982", "Bambang", "Bambang@gmail.com", "3.0", "081234567890");
        mataKuliah = new MataKuliah("CSUI0000", "POK", "Ilmu Komputer");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testRegisterAsdosControllerToRegisterAnAssistant() throws Exception {
        when(registerAsdosService.registerToBecomeAsdos(anyString(), anyString())).thenReturn(mahasiswa);

        mvc.perform(post("/register/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapToJson(mahasiswa)));

    }
}
