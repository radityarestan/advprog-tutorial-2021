package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private Log log;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("2106292982", "Bambang", "Bambang@gmail.com", "3.0", "081234567890");
        log = new Log("2021/04/04 20:00", "2021/04/04 21:00", "Tutorial 1");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testLogControllerPostALog() throws Exception {
        when(logService.createLog(anyString(), any(Log.class))).thenReturn(log);

        mvc.perform(post("/log/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(log)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("Tutorial 1"));
    }

    @Test
    public void testLogControllerGetLogPostMahasiswa() throws Exception {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        logList.add(new Log("2021/04/05 20:00", "2021/04/05 21:00", "Tutorial 2"));
        when(logService.getLogOfMahasiswa(anyString())).thenReturn(logList);

        mvc.perform(get("/log/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(logList)))
                .andExpect(jsonPath("$[1].description").value("Tutorial 2"));
    }

    @Test
    public void testLogControllerGetLogPostById() throws Exception {
        when(logService.getLogByID(anyInt())).thenReturn(log);

        mvc.perform(get("/log/get/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("Tutorial 1"));
    }

    @Test
    public void testLogControllerGetNonExistLog() throws Exception {
        mvc.perform(get("/log/get/100").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLogControllerUpdateLog() throws Exception {
        Log updatedLog = new Log("2021/04/04 20:00", "2021/04/04 21:00", "Tutorial Lab");
        when(logService.updateLog(log.getIdLog(), updatedLog)).thenReturn(updatedLog);
        mvc.perform(put("/log/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON).content(mapToJson(updatedLog)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("Tutorial Lab"));
    }

    @Test
    public void testLogControllerDeleteLog() throws Exception {
        mvc.perform(delete("/log/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testLogControllerGetSummaryMonthlyLog() throws Exception {
        LogSummary logSummary = new LogSummary("May", 2, 700);

        when(logService.getMonthlyLog(anyString(), anyString())).thenReturn(logSummary);
        mvc.perform(get("/log/summary/" + mahasiswa.getNpm() + "/May")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(logSummary)));
    }

    @Test
    public void testLogControllerGetAllSummaryLog() throws Exception {
        List<LogSummary> logSummaryList = new ArrayList<>();
        logSummaryList.add(new LogSummary("May", 2, 700));
        logSummaryList.add(new LogSummary("June", 4, 1400));
        when(logService.getAllLogSummaryByMahasiswa(anyString())).thenReturn(logSummaryList);
        mvc.perform(get("/log/summary/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(logSummaryList)));

    }
}
