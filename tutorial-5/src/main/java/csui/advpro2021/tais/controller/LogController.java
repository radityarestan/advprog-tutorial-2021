package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {

    @Autowired
    LogService logService;

    @PostMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(npm, log));
    }

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getLogsOfMahasiswa(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getLogOfMahasiswa(npm));
    }

    @GetMapping(path = "get/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogById(@PathVariable(value = "id") int id) {
        Log log = logService.getLogByID(id);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @GetMapping(path = "summary/{npm}/{month}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getSummaryMonthlyLogsOfMahasiswa(@PathVariable(value = "npm") String npm,
                                                    @PathVariable(value = "month") String month) {
        return ResponseEntity.ok(logService.getMonthlyLog(npm, month));
    }

    @GetMapping(path = "summary/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<LogSummary>> getSummaryLogsOfMahasiswa(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getAllLogSummaryByMahasiswa(npm));
    }

    @PutMapping(path = "/{logID}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "logID") int logID, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(logID, log));
    }

    @DeleteMapping(path = "/{logID}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "logID") int logID) {
        logService.deleteLogByID(logID);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
