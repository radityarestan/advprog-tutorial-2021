package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.service.RegisterAsdosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/register")
public class RegisterAsdosController {
   @Autowired
   RegisterAsdosService registerAsdosService;

    @PostMapping(path = "/{npm}/{kodeMatkul}", produces = {"application/json"})
    @ResponseBody   
    public ResponseEntity register(@PathVariable(value = "npm") String npm,
                                  @PathVariable(value = "kodeMatkul") String kodeMatkul) {
        return ResponseEntity.ok(registerAsdosService.registerToBecomeAsdos(npm, kodeMatkul));
    }
}
