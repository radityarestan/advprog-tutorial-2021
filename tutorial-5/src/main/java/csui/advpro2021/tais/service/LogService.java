package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;

public interface LogService {
    Log createLog(String npm, Log log);

    Iterable<Log> getLogOfMahasiswa(String npm);

    LogSummary getMonthlyLog(String npm, String month);

    Iterable<LogSummary> getAllLogSummaryByMahasiswa(String npm);

    Log getLogByID(int id);

    Log updateLog(int id, Log log);

    void deleteLogByID(int id);
}
