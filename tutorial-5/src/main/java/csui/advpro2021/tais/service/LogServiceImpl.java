package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormatSymbols;
import java.time.format.TextStyle;
import java.util.*;
import java.time.temporal.ChronoUnit;

@Service
public class LogServiceImpl implements LogService{
    @Autowired
    LogRepository logRepository;

    @Autowired
    MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);

        mahasiswa.getLogList().add(log);
        log.setMahasiswa(mahasiswa);

        mahasiswaRepository.save(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Iterable<Log> getLogOfMahasiswa(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        return mahasiswa.getLogList();
    }

    @Override
    public LogSummary getMonthlyLog(String npm, String month) {
        Iterable<Log> logMahasiswa = getLogOfMahasiswa(npm);
        double workingHour = 0.0;

        for (Log log : logMahasiswa) {
            if (log.getStartTime().getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equalsIgnoreCase(month)) {
                workingHour += ChronoUnit.MINUTES.between(log.getStartTime(), log.getEndTime()) / 60.0;
            }
        }

        double payment = workingHour * 350;

        return new LogSummary(month, workingHour, payment);
    }

    @Override
    public Iterable<LogSummary> getAllLogSummaryByMahasiswa(String npm) {
        double[] workingHoursByMonth = new double[12];
        Iterable<Log> logMahasiswa = getLogOfMahasiswa(npm);

        for (Log log: logMahasiswa) {
            int month = log.getStartTime().getMonthValue();
            workingHoursByMonth[month-1] += ChronoUnit.MINUTES.between(log.getStartTime(), log.getEndTime()) / 60.0;
        }

        List<LogSummary> logSummaryList = new ArrayList<>();
        for (int nthMonth = 0; nthMonth < 12; nthMonth++) {
            if (workingHoursByMonth[nthMonth] == 0.0) continue;
            double payment = workingHoursByMonth[nthMonth] * 350;
            String month = new DateFormatSymbols().getMonths()[nthMonth];
            logSummaryList.add(new LogSummary(month, workingHoursByMonth[nthMonth], payment));
        }
        return logSummaryList;
    }

    @Override
    public Log getLogByID(int id) {
        return logRepository.findById(id);
    }

    @Override
    public Log updateLog(int id, Log log) {
        Log log_user = getLogByID(id);
        log_user.setStartTime(log.getStartTime());
        log_user.setEndTime(log.getEndTime());
        log_user.setDescription(log.getDescription());
        logRepository.save(log_user);
        return log_user;
    }

    @Override
    public void deleteLogByID(int id) {
        logRepository.deleteById(id);
    }
}
