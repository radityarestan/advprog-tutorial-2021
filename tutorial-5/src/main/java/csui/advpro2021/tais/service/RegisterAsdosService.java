package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;

public interface RegisterAsdosService {
    Mahasiswa registerToBecomeAsdos(String npm, String kodeMatkul);
}
