package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisterAsdosServiceImpl implements RegisterAsdosService{
    @Autowired
    MahasiswaRepository mahasiswaRepository;

    @Autowired
    MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa registerToBecomeAsdos(String npm, String kodeMatkul) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        MataKuliah mataKuliah = mataKuliahRepository.findByKodeMatkul(kodeMatkul);

        mahasiswa.setMataKuliah(mataKuliah);
        mataKuliah.getMahasiswaList().add(mahasiswa);

        mahasiswaRepository.save(mahasiswa);
        mataKuliahRepository.save(mataKuliah);
        return mahasiswa;
    }
}
