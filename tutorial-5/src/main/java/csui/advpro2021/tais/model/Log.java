package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "Log")
@Data
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idLog;

    @Column(name = "start_time")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime endTime;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "npm", nullable = false)
    private Mahasiswa mahasiswa;

    public Log(String startTime, String endTime, String description) {
        this.startTime = LocalDateTime.parse(startTime, DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm"));
        this.endTime = LocalDateTime.parse(endTime, DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm"));
        this.description = description;
    }

}
