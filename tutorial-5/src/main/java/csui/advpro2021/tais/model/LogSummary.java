package csui.advpro2021.tais.model;

import lombok.Data;

@Data
public class LogSummary {

    private String month;
    private double workingHours;
    private double payment;

    public LogSummary(String month, double workingHours, double payment) {
        this.month = month;
        this.workingHours = workingHours;
        this.payment = payment;
    }

}
