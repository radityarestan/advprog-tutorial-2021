# Teaching Assistant Information System

Dalam sistem kali ini dibutuhkan 3 entity utama:
- Mahasiswa
- MataKuliah
- Log

Hubungan antar entity yang diminta adalah
- MataKuliah  --1-- menerima  --n--  Mahasiswa
- Mahasiswa   --1-- mempunyai --n-- Log

Fitur yang perlu ditambahkan:
- Setiap mahasiswa dapat membuat, mengubah, maupun menghapus log yang mereka punya.
- Terdapat laporan pembayaran setiap bulan (baik semua maupun per bulannya).
- Laporan pembayaran terdiri dari bulan, jam kerja, dan pembayarannya.  
- Laporan pembayaran berdasarkan jam kerja pada bulan itu dikali dengan 350 (350 Greil/jam).

Ketentuan dari Fitur yang ditambahkan:
- idlog menggunakan tipe data integer (sebagai primary key)
- start dan end menggunakan tipe data datetime
- deskripsi menggunakan tipe data text

Cara penggunaan fitur:  
Menggunakan postman untuk setiap request apakah sudah sesuai yang diminta atau belum