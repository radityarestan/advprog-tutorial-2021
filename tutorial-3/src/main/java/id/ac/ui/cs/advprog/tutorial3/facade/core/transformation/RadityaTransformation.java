package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

public class RadityaTransformation {

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int textLength = text.length();
        int selector = encode ? 2 : -2;
        char[] res = new char[textLength];

        for(int i = 0; i < textLength; i++){
            int newIdx = encode ? (textLength-1-i)+selector:textLength-1-(i+selector);
            res[newIdx % textLength] = text.charAt(i);
        }

        return new Spell(new String(res), codex);
    }
}
