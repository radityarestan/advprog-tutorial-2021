package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private BowRepository bowRepository;

    @Override
    public List<Weapon> findAll() {
        List<Spellbook> spellbooks = spellbookRepository.findAll();
        List<Bow> bows = bowRepository.findAll();

        for (Spellbook spellbook : spellbooks) {
            weaponRepository.save(new SpellbookAdapter(spellbook));
        }

        for (Bow bow : bows) {
            weaponRepository.save(new BowAdapter(bow));
        }

        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if (attackType == 0) {
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " (normal attack): " + weapon.normalAttack());
        } else {
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " (charged attack): " + weapon.chargedAttack());
        }
        weaponRepository.save(weapon);

    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
