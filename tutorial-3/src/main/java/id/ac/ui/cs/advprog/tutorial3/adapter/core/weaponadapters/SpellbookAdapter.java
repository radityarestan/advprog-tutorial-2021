package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isNowLargeSpell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isNowLargeSpell = false;
    }

    @Override
    public String normalAttack() {
        isNowLargeSpell = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        isNowLargeSpell = !isNowLargeSpell;
        if (isNowLargeSpell) {
            return spellbook.largeSpell();
        } else {
            return "Magic power not enough for large spell";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
