package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class Transformation {

    private CelestialTransformation celestialTransformation;
    private AbyssalTransformation abyssalTransformation;
    private RadityaTransformation radityaTransformation;

    public Transformation() {
        celestialTransformation = new CelestialTransformation();
        abyssalTransformation = new AbyssalTransformation();
        radityaTransformation = new RadityaTransformation();
    }

    public String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        spell = celestialTransformation.encode(spell);
        spell = abyssalTransformation.encode(spell);
        spell = radityaTransformation.encode(spell);
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return spell.getText();
    }

    public String decode(String code){
        Spell spell = new Spell(code, RunicCodex.getInstance());
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        spell = radityaTransformation.decode(spell);
        spell = abyssalTransformation.decode(spell);
        spell = celestialTransformation.decode(spell);
        return spell.getText();
    }


}
