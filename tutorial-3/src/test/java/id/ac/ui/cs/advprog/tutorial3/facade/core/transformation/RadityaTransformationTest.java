package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RadityaTransformationTest {
    private Class<?> radityaClass;

    @BeforeEach
    public void setup() throws Exception {
        radityaClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.RadityaTransformation");
    }

    @Test
    public void testRadityaHasEncodeMethod() throws Exception {
        Method translate = radityaClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testRadityaEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "aSdrows ruo egrof ot htimskcalb a ot tnew I dna arif";

        Spell result = new RadityaTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testRadityaHasDecodeMethod() throws Exception {
        Method translate = radityaClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testRadityaDecodesCorrectly() throws Exception {
        String text = "aSdrows ruo egrof ot htimskcalb a ot tnew I dna arif";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new RadityaTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
