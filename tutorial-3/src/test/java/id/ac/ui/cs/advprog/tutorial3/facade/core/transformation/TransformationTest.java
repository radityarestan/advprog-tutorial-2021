package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class TransformationTest {
    private Class<?> transformationClass;
    private Transformation transformation;

    @BeforeEach
    public void setUp() throws Exception {
        transformationClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation");
        transformation = new Transformation();
    }

    @Test
    public void testTransformationIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(transformationClass.getModifiers()));
    }

    @Test
    public void testTransformationHasEncodeMethod() throws Exception {
        Method encodeMethod = transformationClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encodeMethod.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationEncodeShouldBeReturnString() throws Exception {
        String result = transformation.encode("Safira and I went to a blacksmith to forge our sword");
        assertEquals(result.getClass(), String.class);
    }

    @Test
    public void testTransformationEncodedProperly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        String result = transformation.encode(text);
        assertEquals("_D/%SN;FNBDnq@|w%%Z)DFVKCned_DDq&(D<?==b)X{xG!BJJ!#M", result);
    }

    @Test
    public void testTransformationHasDecodeMethod() throws Exception {
        Method decodeMethod = transformationClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decodeMethod.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationDecodeShouldBeReturnString() throws Exception {
        String result = transformation.decode("_D/%SN;FNBDnq@|w%%Z)DFVKCned_DDq&(D<?==b)X{xG!BJJ!#M");
        assertEquals(result.getClass(), String.class);
    }

    @Test
    public void testTransformationDecodedProperly() throws Exception {
        String text = "_D/%SN;FNBDnq@|w%%Z)DFVKCned_DDq&(D<?==b)X{xG!BJJ!#M";
        String result = transformation.decode(text);
        assertEquals("Safira and I went to a blacksmith to forge our sword", result);
    }
}
