package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        this.guild.add(this);
    }

    public void update() {
        if (!guild.getQuestType().equals("Escort")) {
            this.getQuests().add(guild.getQuest());
        }
    }
}
