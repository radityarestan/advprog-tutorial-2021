package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;


    public AdventurerServiceImpl(
            AdventurerRepository adventurerRepository,
            StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {

        Adventurer adventurer = findByAlias(alias);
        AttackBehavior attackBehavior;
        DefenseBehavior defenseBehavior;

        if (attackType.equals("Attack With Gun")){
            attackBehavior = this.strategyRepository.getAttackBehaviorByType("Attack With Gun");
        } else if (attackType.equals("Attack With Magic")) {
            attackBehavior = this.strategyRepository.getAttackBehaviorByType("Attack With Magic");
        } else {
            attackBehavior = this.strategyRepository.getAttackBehaviorByType("Attack With Sword");
        }

        if (defenseType.equals("Defend With Armor")) {
            defenseBehavior = this.strategyRepository.getDefenseBehaviorByType("Defend With Armor");
        } else if (defenseType.equals("Defend With Barrier")) {
            defenseBehavior = this.strategyRepository.getDefenseBehaviorByType("Defend With Barrier");
        } else {
            defenseBehavior = this.strategyRepository.getDefenseBehaviorByType("Defend With Shield");
        }

        adventurer.setAttackBehavior(attackBehavior);
        adventurer.setDefenseBehavior(defenseBehavior);
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
