package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

    public DefendWithBarrier() {}

    public String defend() {
        return "ada barrier nih bos";
    }

    public String getType() {
        return "Defend With Barrier";
    }
}
