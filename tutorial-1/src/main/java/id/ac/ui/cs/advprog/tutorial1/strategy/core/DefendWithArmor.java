package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {

    public DefendWithArmor() {}

    public String defend() {
        return "ada armor nih bos";
    }

    public String getType() {
        return "Defend With Armor";
    }
}
