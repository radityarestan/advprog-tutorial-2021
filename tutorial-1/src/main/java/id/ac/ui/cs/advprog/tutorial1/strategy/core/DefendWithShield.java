package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    public DefendWithShield() {}

    public String defend() {
        return "ada shield ni bos";
    }

    public String getType() {
        return "Defend With Shield";
    }
}
