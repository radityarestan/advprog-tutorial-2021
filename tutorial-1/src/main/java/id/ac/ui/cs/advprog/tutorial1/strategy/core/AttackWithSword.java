package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {

    public AttackWithSword() {}

    public String attack() {
        return "ting ting ting";
    }

    public String getType() {
        return "Attack With Sword";
    }
}
