package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    public AttackWithGun() {}

    public String attack() {
        return "Duar-Duar";
    }

    public String getType() {
        return "Attack With Gun";
    }
}
